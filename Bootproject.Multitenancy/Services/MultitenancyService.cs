﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Service extensions for Multitenancy.
    /// </summary>
    public static class HostExtensions
    {
        /// <summary>
        /// Adds multitenancy to the application.
        /// </summary>
        /// <param name="services">IServiceCollection services</param>
        /// <returns> IServiceCollection</returns>
        public static IServiceCollection AddMultitenancy(this IServiceCollection services)
        {
            //Needed for the Multitenancy, impl. as an extension
            return services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }


        /// <summary>
        /// Use multitenancy.
        /// </summary>
        /// <param name="builder">IApplicationBuilder builder</param>
        public static void UseMultitenancy(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<HostMiddleware>();
            var httpContextAccessor = builder.ApplicationServices.GetRequiredService<IHttpContextAccessor>();
            System.Web.HttpContext.Configure(httpContextAccessor);
        }
    }
}
