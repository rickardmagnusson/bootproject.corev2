﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Invoces the implementation of multitenancy 
    /// </summary>
    public class HostMiddleware
    {
        private readonly RequestDelegate _next;

        public HostMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            //Init Multitenancy
            Host.Init();
            await _next.Invoke(context);
        }
    }
}
