﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Read and bind configuration
    /// </summary>
    public static class ConfigurationReader
    {
        /// <summary>
        /// Get the ConfigurationRoot
        /// </summary>
        /// <param name="basePath">Path to folder containing the file</param>
        /// <returns></returns>
        private static IConfigurationRoot GetConfigurationRoot(this string basePath)
        {
            return new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }

        /// <summary>
        /// Get DomainConfiguration
        /// </summary>
        /// <param name="basePath">Path to folder containing the file</param>
        /// <returns>Bound DomainConfiguration</returns>
        public static DomainConfiguration GetApplicationConfiguration(this string basePath)
        {
            var configuration = new DomainConfiguration();
            var config = GetConfigurationRoot(basePath);

            config
                .GetSection("DomainConfiguration")
                .Bind(configuration);

            return configuration;
        }
    }
}
