﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Holds information about domains
    /// </summary>
    public class DomainConfiguration : IDomainConfiguration
    {
        //A list of domains in this application
        public List<Domain> Domains { get; set; }
    }


    /// <summary>
    /// The Domain(Tenant) object
    /// </summary>
    public class Domain
    {
        /// <summary>
        /// The unique name for this tenant
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Host values, separated with comma(,)
        /// </summary>
        public string Dns { get; set; }

        /// <summary>
        /// Database connectionstring
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// The SessionFactory for this tenant. Is set during init.
        /// </summary>
        public ISessionFactory SessionFactory { get; set; }
    }

    /// <summary>
    /// IDomainConfiguration interface
    /// </summary>
    public interface IDomainConfiguration
    {
        List<Domain> Domains { get; set; }
    }
}
