﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Convert the Table name to the provided name set above class.
    /// Default is the class name.
    /// </summary>
    public class TableNameConvention : IClassConvention, IClassConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IClassInspector> criteria)
        {
            criteria.Expect(x => !string.IsNullOrEmpty(x.TableName));
        }

        public void Apply(IClassInstance instance)
        {
            Type T = instance.EntityType;
            var attribute = T.GetCustomAttribute<TableAttribute>(true);

            if (attribute != null)
                instance.Table(attribute.Name);
        }
    }
}
