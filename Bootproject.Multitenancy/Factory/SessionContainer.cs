﻿using NHibernate;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Create Domain objects
    /// </summary>
    public class SessionContainer
    {
        /// <summary>
        /// Returns the current SessionContainer
        /// </summary>
        public static SessionContainer Current { get; private set; }

        //Singleton Lock
        private static readonly object Lock = new object();
        
        //Domain objects
        private Dictionary<string, Domain> Cache { get; set; } 

        /// <summary>
        /// Singleton instance of SessionContainer
        /// </summary>
        static SessionContainer()
        {
            lock (Lock)
            {
                if(Current == null)
                {
                    Debug.WriteLine("Current is null");
                    Current = new SessionContainer();
                    Current.Cache = new Dictionary<string, Domain>();
                }
            }
        }

        /// <summary>
        /// Singleton SessionContainer.
        /// Avoid new() instance.
        /// </summary>
        private SessionContainer()
        {
        }

        /// <summary>
        /// Creates all Tenants from configuration
        /// </summary>
        public void CreateContainer()
        {
            if(Cache.Count == 0) //Only build if not built.
                /*
                    Build the configuration domains from appsettings.
                    Add domain configuration to a Domain object.
                    Add SessionFactory to Domain object.
                 */
                Directory
                  .GetCurrentDirectory()
                  .GetApplicationConfiguration()
                  .Domains.ForEach(t =>
                  {
                      if (!Current.Cache.ContainsKey(t.Name))
                      {
                          Debug.WriteLine( $"Domain:{t.Name}");
                          /*
                             Add nHibernate SessionFactory to this domain object.
                          */
                          t.SessionFactory = new SessionFactory(t.ConnectionString).CreateConfig();
                          Current.Cache.Add(t.Name, t);
                      }
                  });
        }

        /// <summary>
        /// Returns a SessionFactory for the current domain.
        /// </summary>
        public ISessionFactory SessionFactory
        {
            get
            {
                var dns = GetCurrentDomain();
                return Current.Cache[dns.Name].SessionFactory;
            }
        }

        public Domain GetCurrentDomain()
        {
            //Returns only the host name without port
            var domain = HttpContext.Current.Request.Host.Host;

            foreach(var d in Cache.ToList())
            {
                var domains = SplitLine(d.Value.Dns);
                if (domains.Contains(domain))
                    return d.Value;
            }

            //Return localhost if no domain was found.
            //There must be at least one localhost in configuration.
            //this will throw an exception if not found.
            var defaultDomain = Cache.ToList().FirstOrDefault(c => c.Value.Dns.Contains("localhost")).Value;
            if (defaultDomain == null)
                throw new System.Exception("Specify at least one Dns as localhost in appsettings.json");
            else
                return defaultDomain;
        }

        private List<string> SplitLine(string line)
        {
            return line.Contains(',') ? 
                line.Split(',').ToList() : 
                new List<string> { line };
        }
    }
}
