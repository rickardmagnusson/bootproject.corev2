﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Contains functions to build nHibenate configuration
    /// </summary>
    public class SessionFactory
    {
        private string ConnectionString { get; set; }

        /// <summary>
        /// Create instance with ConnectionString
        /// </summary>
        /// <param name="connectionString"></param>
        public SessionFactory(string connectionString)
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// Creates ISessionFactory
        /// </summary>
        /// <returns>ISessionFactory</returns>
        public ISessionFactory CreateConfig()
        {
            return FluentConfiguration
                .BuildSessionFactory();
        }

        /// <summary>
        /// Build the fluent(nHibernate) configuration
        /// </summary>
        public FluentConfiguration FluentConfiguration
        {
            get
            {
                return Fluently
                  .Configure()
                     .Database(DatabaseConfiguration)
                        .Mappings(MapAssemblies)
                           .ExposeConfiguration(BuildSchema)
                              .ExposeConfiguration(c => c.SetInterceptor(new SqlPrintInterceptor()));
            }
        }



        /// <summary>
        /// Searches all assemblies for IEntity's
        /// </summary>
        /// <param name="fmc">MappingConfiguration</param>
        internal void MapAssemblies(MappingConfiguration fmc)
        {
            (from a in AppDomain.CurrentDomain.GetAssemblies()
             select a
                 into assemblies
                    select assemblies)
                        .ToList()
                            .ForEach(a => {
                                fmc.AutoMappings.Add(AutoMap.Assembly(a)
                                    .Conventions.AddFromAssemblyOf<TableNameConvention>()
                                    .Conventions.Add<StringColumnLengthConvention>()
                                        .OverrideAll(p => {
                                            p.SkipProperty(typeof(NoProperty));
                                        }).Where(IsEntity));
                            });
        }


        /// <summary>
        /// Creates the configuration for FluentnHibernate
        /// </summary>
        /// <returns>IPersistenceConfigurer config</returns>
        internal IPersistenceConfigurer DatabaseConfiguration()
        {
            return MySQLConfiguration.Standard
                    .UseOuterJoin()
                    .ConnectionString(ConnectionString);
        }


        /// <summary>
        /// The Entity to look for
        /// </summary>
        /// <param name="t">The type to compare</param>
        /// <returns>If type is Entity</returns>
        private static bool IsEntity(Type t)
        {
            return typeof(IEntity).IsAssignableFrom(t);
        }


        /// <summary>
        /// Builds the configuration for FluentnHibernate/nHibernate
        /// </summary>
        /// <param name="config"></param>
        internal void BuildSchema(NHibernate.Cfg.Configuration config)
        {
#if DEBUG
            SchemaMetadataUpdater.QuoteTableAndColumns(config, dialect: new NHibernate.Dialect.GenericDialect());
            string path = Directory.GetCurrentDirectory() + "\\FluentConfiguration.xml";

            new SchemaExport(config).SetDelimiter(";").SetOutputFile(path).Create(true, false);
            new SchemaUpdate(config).Execute(false, true);
#endif
        }

        /// <summary>
        /// Adds Sql output to a textfile.
        /// </summary>
        public class SqlPrintInterceptor : EmptyInterceptor
        {
            public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
            {
#if DEBUG
                var sqlfile = Directory.GetCurrentDirectory() + "\\sqloutput.txt";
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(sqlfile, true))
                    file.Write(Environment.NewLine + sql.ToString());

                Trace.WriteLine(sql.ToString());
#endif
                return sql;
            }
        }
    }
}
