﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Domain object.
    /// Inherit IEntity to create a domain object
    /// </summary>
    public interface IEntity
    {
    }
}
