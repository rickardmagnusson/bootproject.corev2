﻿
using NHibernate;

namespace Bootproject.Multitenancy
{
    /// <summary>
    /// Host class for tenants.
    /// </summary>
    public class Host
    {
        /// <summary>
        /// Init multitenancy and creates configuration.
        /// </summary>
        public static void Init()
        {
            SessionContainer.Current.CreateContainer();
        }

        /// <summary>
        /// Opens the ISession.
        /// </summary>
        /// <returns>The ISession created</returns>
        public static ISession Open()
        {
            return SessionContainer.Current.SessionFactory.OpenSession();
        }

        /// <summary>
        /// Avoid instance of class.
        /// </summary>
        private Host() { }

        /// <summary>
        /// Singleton ctor().
        /// </summary>
        static Host() { }
    }
}
