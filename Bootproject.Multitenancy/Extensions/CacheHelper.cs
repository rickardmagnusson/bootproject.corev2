﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using System.Text;

namespace Bootproject.Multitenancy
{
    public class CacheHelper
    {
        private readonly static object Lock = new object();

        private static CacheHelper Factory { get; set; }

        private readonly Dictionary<string, MemoryCache> dict = new Dictionary<string, MemoryCache>();

        private CacheHelper()
        {
        }

        public MemoryCache Get()
        {
            var domain = SessionContainer.Current.GetCurrentDomain().Name;
          
            if (!Cache.dict.ContainsKey(domain))
                Cache.dict.Add(domain, new MemoryCache(domain));

            return Cache.dict[domain];
        }

        public static CacheHelper Cache
        {
            get
            {
                return Factory;
            }
        }

        static CacheHelper()
        {
            lock (Lock)
                Factory = new CacheHelper();
        }

    }
}
