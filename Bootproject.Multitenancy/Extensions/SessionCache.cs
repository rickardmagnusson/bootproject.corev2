﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using System.Text;

namespace Bootproject.Multitenancy
{
    public class SessionCache
    {
        private static readonly object Lock = new object();
        private static SessionCache Factory { get; set; }
        private MemoryCache memoryCache { get; set; }

        private SessionCache()
        {         
        }

        public SessionCache Current(string name)
        {
            memoryCache = new MemoryCache(name);
            return Factory;
        }

        static SessionCache()
        {
            lock (Lock)
                Factory = new SessionCache();
        }


        public static MemoryCache Cache
        {
            get
            {
                return Factory.memoryCache;
            }
        }
    }
}
