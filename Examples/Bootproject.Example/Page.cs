﻿
using Bootproject.Multitenancy;
using System.ComponentModel.DataAnnotations.Schema;


namespace Bootproject.Example
{
    [Table("Pages")]
    public class Page : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
    }
}
