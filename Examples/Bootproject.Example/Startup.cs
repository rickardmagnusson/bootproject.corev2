﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Bootproject.Multitenancy;
using Microsoft.AspNetCore.Mvc;

namespace Bootproject.Example
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMultitenancy();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseMultitenancy();
            app.Run(async (context) =>
            {
                //Add a page to table Pages before debug!!
                var title = Host.Open().All<Page>(1).Title;
                string html = $"<html>\n<head>\n<title>{title}</title>\n</head>\n<body>\n\t<h1>Tenant (Page) name: {title}</h1>\n</body>\n</html>";

                await context.Response.WriteAsync(html);
            });
        }
    }
}
