﻿# Bootproject.Examples

## Minimal Web application
    This project contains a minimal example of 
    using multitenancy in your project.

### Project

    1. Added Reference to Bootproject.Multitenancy.Core from NuGet
    2. Added DomainConfiguration in (See) appsettings.json
    3. Added (See page class) Page model
    4. Added Startup middleware (See startup)