## Bootproject.Multitenancy (Core 3.1)

This project is an extension of nHibernate's ISessionFactory and makes it incredible 
easy to use multiple sessions within the same application. It's used to 
define different domains and their databases. Uses MySql as database.

Since the previous versions in .Net 4.6 this is more lightweighted, without  
lack of functionallity.

Built in model creation. When adding a new model its automatically gets 
replicated in database.

### Setup

Reference Multitenancy to your project, and in Startup.cs add:
	

Add in ConfigureServices

```csharp
services.AddMultitenancy();
```

And in Configure before app.UseMvc

```csharp
app.UseMultitenancy();
```

### Configuration
Add "DomainConfiguration" section to appsettings.json like the example below:
   
    {
	  "Logging": {
		"LogLevel": {
		  "Default": "Warning"
		}
	  },
	  "DomainConfiguration": {
		"Domains": [
		  {
			"Name": "bootproject",
			"Dns": "localhost,bootproject.net",
			"ConnectionString": "server=localhost;port=3306;database=test1;uid=root;password=1234"
		  },
		  {
			"Name": "example",
			"Dns": "example.com",
			"ConnectionString": "server=localhost;port=3306;database=test2;uid=root;password=1234"
		  }
		]
	  },
	  "AllowedHosts": "*"
	}


 *Note: - Default domain will always be localhost.*


## How to use


Adding a model:	

```csharp
[Table("Pages")]
public class Page : IEntity
{
	public virtual int Id{ get; set;}
	public virtual string Title{ get; set}
	//.....
}
	
//In controller
private readonly ISession Session; 
	
public HomeController()
{
	Session = Host.Open();
}
	
public IActionResult Index()
{
	var model = Session.All<Page>();
	
	if (!model.Any()) { 
	    var page = new Page { Id=1, Title="Start" };
	    Session.Save<Page>(page);
	    Session.Flush();
	}
	return View(model);
} 
```

### Caching

In Bootproject.Multitenancy project theres built in cache of Entities.
When for eg. calling All<T>.First(t=> t.Id==1) wich makes a call for 
all T could be a performance issue, but since its already in the cache, 
this is not a problem. 